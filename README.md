CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Vacuum profile brings an absolutely bare Drupal installation. Less than
the minimal profile provided by Drupal core, it comes with no configuration at
all and does not declare any dependencies.

By design, Drupal requires a profile in order to be installed. The Vacuum
profile is the most similar to having no installation profile.

It is intended for Drupal installations from existing configuration or from a
custom base module that provides the initial configuration and dependencies.

Note that an installation based on the Vacuum profile alone is almost not
usable and does not fulfill with accessibility rules in any case.


REQUIREMENTS
------------

Just Drupal core.


INSTALLATION
------------

Download and choose the Vacuum profile at the Drupal installation process. See
https://www.drupal.org/node/2743413 for further information.


CONFIGURATION
-------------

The Vacuum profile does not have any configuration.


MAINTAINERS
-----------

Current maintainers:

  * Manuel Adan (manuel.adan) - https://www.drupal.org/u/manueladan
